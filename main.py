#-*- coding: utf-8 -*-
# Creation Date : 2019-11-27
# Created by : Antoine LeBel
from fastapi import FastAPI
from pymongo import MongoClient
from bson.json_util import dumps,loads
from models.Person import Person

from tp3 import db

app = FastAPI()
DB = db.DB()
DB.connect("usertp3", "usertp3")

@app.post("/addperson")
def addperson(person:Person):
    DB.save({
        "name": person.name,
        "address": person.address,
        "profession": person.profession
    })
    print(person)

    return {"message" : "success",
    "person":person}

@app.get("/findbyname/")
def findByName(name : str):
    response = DB.find(name)
    return {dumps(response[0])}

@app.delete("/deletebyname/")
def deletebyname(name : str):
    DB.delete(name)
    return {"delete" : "success"}
@app.get("/findall")
def findall():
    allPerson = DB.finAll()
    return {dumps(allPerson)}
