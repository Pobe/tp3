# TP3

## Definition Of Done
Important dans un contexte BDD selon nous.
## User stories

### Exemple
Titre:mon titre

Personne: En tant qu'utilisateur je dois.

Besoin: mon besoin

Raison: les raison

Critères d'acceptation:

Pour que Exemple fonctionne, il faut que ces critères soient acceptés

Un mock de la base de données et de l'API doivent faire preuve du bon fonctionnement des tests unitaires.

### Avoir un prototype où il est possible de sauvegarder dans la base de données
Titre: Prototype de formation

Action: En tant que membre de l'équipe.

Besoin: Le membre doit savoir comment MongoDB, FastAPI et les tests unitaires fonctionnent. 

Raison: Pour que l'on puisse séparer efficacement les tâches et ainsi les standardisées.

Critère d'acception

Afin d'assurer que les membres de l'équipe soit au bon niveau.

Le membre doit faire un prototype qui permet un appel sur le chemin "/"

et il doit à ce chemin sauvegarder un objet dans la base de données

et il doit prouver par un mock de la BD et de l'API que les tests unitaires fonctionnent bien avec "nosetests"
