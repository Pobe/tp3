from behave import given, when, then, step
import requests

api_endpoints = {}
request_headers = {}
response_codes ={}
response_texts={}
request_bodies = {}
api_url=None

@given(u'I set REST API url')
def step_impl(context):
    global api_url
    api_url = 'http://127.0.0.1:8000/'

# START POST Scenario
@given(u'I Set posts api endpoint')
def step_impl(context):
    api_endpoints['POST_URL'] = api_url+'addperson'

@when(u'I Set HEADER param request content type as "{header_conent_type}"')
def step_impl(context, header_conent_type):
    request_headers['content-type'] = header_conent_type

@when(u'Set request Body')
def step_impl(context):
    request_bodies['POST']={"name": "safiou","address": "montreal","profession": 'etudiant'}

@when(u'Send a POST HTTP request')
def step_impl(context):
    response = requests.post(url=api_endpoints['POST_URL'], json=request_bodies['POST'], headers=request_headers)
    response_texts['POST']=response.text
    print("post response :"+response.text)
    statuscode = response.status_code
    response_codes['POST'] = statuscode

@then(u'I receive valid HTTP response code 200')
def step_impl(context):
    print('Post rep code ;'+str(response_codes['POST']))
    assert response_codes['POST'] is 200
# END POST Scenario

# START GET Scenario
@given(u'I Set GET api endpoint "{endpoint}"')
def step_impl(context,endpoint):
    api_endpoints['GET_URL'] = api_url+endpoint+'/'+"?name=safiou"

@when(u'Send GET HTTP request')
def step_impl(context):
    response = requests.get(url=api_endpoints['GET_URL'], headers=request_headers)
    response_texts['GET'] = response.text
    statuscode = response.status_code
    response_codes['GET'] = statuscode

@then(u'I receive valid HTTP response code 200 for "{request_name}"')
def step_impl(context,request_name):
    print('Get rep code for '+request_name+':'+ str(response_codes[request_name]))
    assert response_codes[request_name] is 200

@then(u'Response BODY "{request_name}" is non-empty')
def step_impl(context,request_name):
    print('request_name: '+request_name)
    print(response_texts)
    assert response_texts[request_name] is not None
# END GET Scenario