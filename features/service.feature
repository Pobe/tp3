Feature: Test post and get methods REST API 

  Background:
    Given I set REST API url

  Scenario: Add person on data base
    Given I Set posts api endpoint
    When I Set HEADER param request content type as "application/json."
    And Set request Body
    And Send a POST HTTP request
    Then I receive valid HTTP response code 200
    And Response BODY "POST" is non-empty

  Scenario: Find person on data base
    Given I Set GET api endpoint "findbyname"
    When I Set HEADER param request content type as "application/json."
    And Send GET HTTP request
    Then I receive valid HTTP response code 200 for "GET"
    And Response BODY "GET" is non-empty
