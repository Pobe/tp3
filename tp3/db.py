# -*- coding: utf-8 -*-
# Creation Date : 2019-11-27
# Created by : Antoine LeBel
from pymongo import MongoClient
class DB():
    # TODO put this in config
    #DB is included in connection
    URL = "mongodb://{}:{}@ds149218.mlab.com:49218/tp3"
    COLL = "mycollection"

    def __init__(self):
        self.collection = None
        self.user = None
        self.password = None
        self._client = None

    def connect(self, user, password):
        self.user = user
        self.password = password
        self._client = MongoClient(self.URL.format(
            self.user, self.password), retryWrites=False)
        self.collection = self._client["tp3"][self.COLL]
        print(type(self.collection))

    def save(self, obj):
        """
        Uses exit code.
        0: success
        1: error
        """
        if not self.collection:
            print("Connect to collection before saving.")
            return 1
        self.collection.insert_one(obj)
        return obj

    def finAll(self):
        lst = []
        for x in self.collection.find():
            lst.append(x)
        return lst
    def find(self,name : str):
        myquery = { "name": name }
        #print(name)
        lst = []
        for x in self.collection.find(myquery):
            lst.append(x)     
        return lst
    def delete(self,name : str):
        myquery = { "name": name }
        self.collection.delete_one(myquery)