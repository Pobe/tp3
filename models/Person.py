from pydantic import BaseModel

class Person(BaseModel):
    name: str
    address: str #= None
    profession: str